﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DocumentCategory_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                LoadData(db);
            }
        }

    }

    private void LoadData(DataClassesDatabaseDataContext db)
    {
        ControllerDocumentCategory position = new ControllerDocumentCategory(db);
        repeaterCategory.DataSource = position.Data();
        repeaterCategory.DataBind();
    }


    protected void repeaterCategory_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/DocumentCategory/Form.aspx?uid=" + e.CommandArgument.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                var category = db.Table_DocumentCategories.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());

                db.Table_DocumentCategories.DeleteOnSubmit(category);
                db.SubmitChanges();

                LoadData(db);
            }
        }
    }
}