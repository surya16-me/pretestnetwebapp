﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ControllerDocument
/// </summary>
public class ControllerDocument : ClassBase
{
    public ControllerDocument(DataClassesDatabaseDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void ViewData()
    {
        db.Table_Documents.Select(x => new
        {
            x.ID,
            x.UID,
            x.Name,
            NameCompany = x.Table_Company.Name,
            NameCategory = x.Table_DocumentCategory.Name,
            x.Description
            

        }).ToArray();
    }

    public Table_Document Create(int IDCompany, int IDCategory, string name, string description)
    {
        Table_Document document = new Table_Document
        {
            UID = Guid.NewGuid(),
            IDCompany = IDCompany,
            IDCategory = IDCategory,
            Name = name,
            Description = description,
            Flag = 1,
            CreatedBy = 1,
            CreatedAt = DateTime.Now,

        };

        db.Table_Documents.InsertOnSubmit(document);
        return document;
    }
    public Table_Document Cari(int ID)
    {
        return db.Table_Documents.FirstOrDefault(x => x.ID == ID);
    }

    public Table_Document Update(int ID, int IDCompany, int IDCategory, string name, string description)
    {
        var document = Cari(ID);

        if (document != null)
        {
            document.IDCompany = IDCompany;
            document.IDCategory = IDCategory;
            document.Name = name;
            document.Description = description;
            document.Flag = 1;
            document.CreatedBy = 1;


            return document;
        }
        else
        {
            return null;
        }
    }
    public Table_Document Delete(int ID)
    {
        var document = Cari(ID);
        if (document != null)
        {
            db.Table_Documents.DeleteOnSubmit(document);
            db.SubmitChanges();
            return document;
        }
        else
        {
            return null;
        }
    }
}