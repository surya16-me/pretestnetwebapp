﻿using AjaxControlToolkit.HtmlEditor.ToolbarButtons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.PeerToPeer;
using System.Net;
using System.Web;
using System.Xml.Linq;

/// <summary>
/// Summary description for ControllerUser
/// </summary>
public class ControllerUser : ClassBase
{
    public ControllerUser(DataClassesDatabaseDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void ViewData()
    {
        db.Table_Users.Select(x => new
        {
            x.ID,
            x.UID,
            x.Name,
            NameCompany = x.Table_Company.Name,
            NamePosition = x.Table_Position.Name,
            x.Address,
            x.Email,
            x.Telephone

        }).ToArray();
    }

    public Table_User Create(int IDCompany, int IDPosition, string name, string address, string email, string telephone, string username, string password)
    {
        Table_User tBUser = new Table_User
        {
            UID = Guid.NewGuid(),
            IDCompany = IDCompany,
            IDPosition = IDPosition,
            Name = name,
            Address = address,
            Email = email,
            Telephone = telephone,
            Username = username,
            Password = password,
            Role = "Admin",
            Flag = 1,
            CreatedBy = 1,
            CreatedAt = DateTime.Now,

        };

        db.Table_Users.InsertOnSubmit(tBUser);
        return tBUser;
    }

    public Table_User Cari(int ID)
    {
        return db.Table_Users.FirstOrDefault(x => x.ID == ID);

    }
    public Table_User Update(int ID, int IDCompany, int IDPosition, string name, string address, string email, string telephone, string username, string password)
    {
        var user = Cari(ID);

        if (user != null)
        {
            user.IDCompany = IDCompany;
            user.IDPosition = IDPosition;
            user.Name = name;
            user.Address = address;
            user.Email = email;
            user.Telephone = telephone;
            user.Username = username;
            user.Password = password;
            user.Role = "Admin";
            user.Flag = 1;
            user.CreatedBy = 1;


            return user;
        }
        else
        {
            return null;
        }
    }

    public Table_User Delete(int ID)
    {
        var user = Cari(ID);
        if (user != null)
        {
            db.Table_Users.DeleteOnSubmit(user);
            db.SubmitChanges();
            return user;
        }
        else
        {
            return null;
        }
    }
}